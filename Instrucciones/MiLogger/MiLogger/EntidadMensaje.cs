﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiLogger
{
    public class EntidadMensaje
    {
        public string mensajeLog { get; set; }
        
        public Enumerado.TipoMensaje tipoMensaje { get; set; }
    }
}
