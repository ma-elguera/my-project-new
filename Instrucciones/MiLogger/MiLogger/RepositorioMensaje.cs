﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiLogger
{
    public class RepositorioMensaje : IRepositorio
    {
        public void LogMensaje(string mensajeLog)
        {
            if (string.IsNullOrEmpty(mensajeLog.Trim()))
            {
                return;
            }
            
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["ConnectionString"]);
            connection.Open();
            
            SqlCommand command = new SqlCommand("Insert into Log Values('" + mensajeLog + "', " + Enumerado.TipoMensaje.Mensaje + ")");
            command.ExecuteNonQuery();

            string mensajeAGuardar = string.Empty;

            if (!File.Exists(System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"] + "ArchivoLog" + DateTime.Now.ToShortDateString() + ".txt"))
            {
                mensajeAGuardar += File.ReadAllText(System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"] + "ArchivoLog" + DateTime.Now.ToShortDateString() + ".txt");
            }

            mensajeAGuardar += DateTime.Now.ToShortDateString() + mensajeLog;

            File.WriteAllText(System.Configuration.ConfigurationManager.AppSettings["CarpetaDeLog"] + "ArchivoLog" + DateTime.Now.ToShortDateString() + ".txt", mensajeAGuardar);
            
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine(DateTime.Now.ToShortDateString() + mensajeLog);
        }
    }
}
