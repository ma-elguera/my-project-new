﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiLogger
{
    public class Enumerado
    {
        public enum TipoMensaje
        {
            Mensaje = 1,
            Error = 2,
            Alerta = 3
        };

        public enum TipoRepositorio
        {
            Archivo = 1,
            Consola = 2,
            BaseDatos = 3
        };
    }
}
